# SPDX-FileCopyrightText: 2022 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM debian:bullseye-slim
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -qq update && apt-get -qq -y install --no-install-recommends \
    curl \
    ca-certificates \
    gnupg \
    rsyslog \
    && rm -rf /var/lib/apt/lists/*
RUN echo "deb http://apt.postgresql.org/pub/repos/apt bullseye-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN curl --output /etc/apt/trusted.gpg.d/pgdg.asc https://www.postgresql.org/media/keys/ACCC4CF8.asc
RUN mkdir -p /etc/postgresql-common/createcluster.d/
RUN echo "create_main_cluster = false" > /etc/postgresql-common/createcluster.d/no-main-cluster.conf
RUN apt-get -qq update && apt-get -y install --no-install-recommends \
    postgresql-15 \
    patroni \
    && rm -rf /var/lib/apt/lists/*
RUN curl --location --output /usr/local/bin/pglift \
    https://gitlab.com/api/v4/projects/dalibo%2Fpglift/packages/generic/bin/v0.29.0a2/pglift
RUN chmod a+rx /usr/local/bin/pglift

RUN useradd --home-dir /var/lib/pglift --create-home pglift

RUN mkdir -p /etc/pglift/postgresql
COPY settings.yaml /etc/pglift/
COPY pg_hba.conf /etc/pglift/postgresql/
COPY rsyslog.conf /etc/
RUN install --directory --owner pglift /var/lib/pglift/log/patroni/15-main
RUN install --owner pglift /dev/null /var/lib/pglift/log/patroni/15-main/patroni.log

COPY start.sh /
RUN chmod +x /start.sh
WORKDIR /var/lib/pglift
CMD ["/start.sh"]
