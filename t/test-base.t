# SPDX-FileCopyrightText: 2024 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

Base tests for the command line interface

  $ pglift --version
  pglift version (\d\.).* (re)

Calling --version does not load site settings

  $ env SETTINGS='in va lid' pglift --version
  pglift version (\d\.).* (re)

Calling 'site-settings' loads site settings

  $ env SETTINGS='in va lid' pglift site-settings
  Error: invalid site settings
  Expecting value: line 1 column 1 (char 0)
  [1]

Calling --help load site settings (because all commands need to be collected)

  $ env SETTINGS='not a settings' pglift --help
  Error: invalid site settings
  Expecting value: line 1 column 1 (char 0)
  [1]
