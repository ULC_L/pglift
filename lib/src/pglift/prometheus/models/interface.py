# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from functools import partial
from typing import Annotated, Final, Literal, Optional

from pydantic import AfterValidator, Field, SecretStr

from ... import types
from ...models.helpers import check_conninfo, check_excludes
from ...types import Port

default_port: Final = 9187


def check_password(v: Optional[SecretStr]) -> Optional[SecretStr]:
    """Validate 'password' field.

    >>> Service(password='without_space')  # doctest: +ELLIPSIS
    Service(...)
    >>> Service(password='with space')  # doctest: +ELLIPSIS
    Traceback (most recent call last):
      ...
    pydantic_core._pydantic_core.ValidationError: 1 validation error for Service
    password
      Value error, password must not contain blank spaces [type=value_error, input_value='with space', input_type=str]
        ...
    """
    # Avoid spaces as this will break postgres_exporter configuration.
    # See https://github.com/prometheus-community/postgres_exporter/issues/393
    if v is not None and " " in v.get_secret_value():
        raise ValueError("password must not contain blank spaces")
    return v


class Service(types.Service, service_name="prometheus"):
    port: Annotated[
        Port,
        Field(
            description="TCP port for the web interface and telemetry of Prometheus",
            validate_default=True,
        ),
    ] = default_port
    password: Annotated[
        Optional[SecretStr],
        Field(
            description="Password of PostgreSQL role for Prometheus postgres_exporter.",
            exclude=True,
        ),
        AfterValidator(check_password),
    ] = None


class PostgresExporter(types.BaseModel):
    """Prometheus postgres_exporter service."""

    """
    >>> PostgresExporter(name='without-slash', dsn="", port=12)  # doctest: +ELLIPSIS
    PostgresExporter(name='without-slash', ...)
    >>> PostgresExporter(name='with/slash', dsn="", port=12)
    Traceback (most recent call last):
      ...
    pydantic_core._pydantic_core.ValidationError: 1 validation error for PostgresExporter
    name
      Value error, must not contain slashes [type=value_error, input_value='with/slash', input_type=str]
        ...
    """

    name: Annotated[
        str,
        Field(description="locally unique identifier of the service"),
        AfterValidator(partial(check_excludes, ("/", "slashes"))),
    ]
    dsn: Annotated[
        str,
        Field(description="connection string of target instance"),
        AfterValidator(partial(check_conninfo, exclude=[])),
    ]
    password: Annotated[
        Optional[SecretStr], Field(description="connection password")
    ] = None
    port: Annotated[
        types.Port, Field(description="TCP port for the web interface and telemetry")
    ]
    state: Annotated[
        Literal["started", "stopped", "absent"],
        types.CLIConfig(choices=["started", "stopped"]),
        Field(description="runtime state"),
    ] = "started"
