# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path
from typing import TYPE_CHECKING, Any, Protocol, TypeVar

if TYPE_CHECKING:
    from pydantic import BaseModel


@dataclass
class Certificate:
    path: Path
    private_key: Path

    def __post_init__(self) -> None:
        if not self.path.exists():
            raise ValueError(f"path={self.path} does not exist")
        if not self.private_key.exists():
            raise ValueError(f"private_key={self.private_key} does not exist")


class CertFactory(Protocol):
    def __call__(
        self, *identities: str, common_name: str | None = None
    ) -> Certificate: ...


M = TypeVar("M", bound="BaseModel")


def model_copy_validate(model: M, update: dict[str, Any]) -> M:
    """Like BaseModel.copy(), but with validation (and default value setting)."""
    return model.__class__.model_validate(
        dict(model.model_dump(by_alias=True), **update)
    )
