# SPDX-FileCopyrightText: 2024 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from pathlib import Path

import pytest

from pglift_cli._settings import Settings


def test_settings(tmp_path: Path, bindir_template: str) -> None:
    s = Settings.model_validate(
        dict(prefix="/", postgresql={"bindir": bindir_template})
    )
    assert s.cli.logpath is None

    with pytest.warns(FutureWarning, match="logpath' setting is deprecated"):
        s = Settings.model_validate(
            dict(
                prefix="/",
                postgresql={"bindir": bindir_template},
                cli={"logpath": "/tmp/logs"},
            )
        )
    assert str(s.cli.logpath) == "/tmp/logs"
