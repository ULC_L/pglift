# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import socket
from pathlib import Path
from typing import Annotated, Any

import port_for
import psycopg.conninfo
import pydantic
import pytest

from pglift import plugin_manager, types
from pglift.models import interface
from pglift.prometheus.models import interface as prometheus_models
from pglift.settings import Settings

p1 = port_for.select_random()
p2 = port_for.select_random()


def short_str(value: str) -> str:
    if len(value) > 2:
        raise AssertionError(f"string is too long: {value}")
    return value


class S(types.BaseModel):
    name: Annotated[str, pydantic.AfterValidator(short_str)] = "ab"
    s_port: Annotated[types.Port, pydantic.Field(validate_default=True)] = p2


class M(types.BaseModel):
    m_port: Annotated[types.Port, pydantic.Field(validate_default=True)] = p1
    s: Annotated[S, pydantic.Field(default_factory=dict, validate_default=True)]


@pytest.mark.parametrize(
    "obj, is_valid, expected_errors",
    [
        (
            {"m_port": p1, "s": {"s_port": p2}},
            True,
            [
                {
                    "input": p1,
                    "loc": ("m_port",),
                    "msg": f"Value error, port {p1} already in use",
                    "type": "value_error",
                },
                {
                    "input": p2,
                    "loc": ("s", "s_port"),
                    "msg": f"Value error, port {p2} already in use",
                    "type": "value_error",
                },
            ],
        ),
        (
            {"s": {"name": "xyz"}},
            False,
            [
                {
                    "input": p1,
                    "loc": ("m_port",),
                    "msg": f"Value error, port {p1} already in use",
                    "type": "value_error",
                },
                {
                    "input": "xyz",
                    "loc": ("s", "name"),
                    "msg": "Assertion failed, string is too long: xyz",
                    "type": "assertion_error",
                },
                {
                    "type": "value_error",
                    "loc": ("s", "s_port"),
                    "msg": f"Value error, port {p2} already in use",
                    "input": p2,
                },
            ],
        ),
        (
            {},
            True,
            [
                {
                    "type": "value_error",
                    "loc": ("m_port",),
                    "msg": f"Value error, port {p1} already in use",
                    "input": p1,
                },
                {
                    "type": "value_error",
                    "loc": ("s", "s_port"),
                    "msg": f"Value error, port {p2} already in use",
                    "input": p2,
                },
            ],
        ),
    ],
    ids=["specified values", "default ports", "default values"],
)
def test_port_validator(
    obj: Any, is_valid: bool, expected_errors: list[dict[str, Any]]
) -> None:
    if is_valid:
        with types.validation_context(operation="create"):
            m = M.model_validate(obj)
        assert m.model_dump() == {
            "m_port": p1,
            "s": {"name": "ab", "s_port": p2},
        }

    def filter_error(d: Any) -> Any:
        del d["ctx"]
        del d["url"]
        return d

    with socket.socket() as s1, socket.socket() as s2:
        s1.bind(("", p1))
        s1.listen()
        s2.bind(("", p2))
        s2.listen()
        with types.validation_context(operation="create"):
            with pytest.raises(pydantic.ValidationError) as cm:
                M.model_validate(obj)
        errors = cm.value.errors()
        assert [filter_error(e) for e in errors] == expected_errors

        if is_valid:
            with types.validation_context(operation="update"):
                m = M.model_validate(obj)
            assert m.model_dump() == {"m_port": p1, "s": {"name": "ab", "s_port": p2}}


def test_composite_instance_model(
    composite_instance_model: type[interface.Instance],
) -> None:
    assert composite_instance_model.model_fields["pgbackrest"].is_required()
    assert not composite_instance_model.model_fields["patroni"].is_required()


def test_instance_version(settings: Settings, pg_version: str) -> None:
    with types.validation_context(operation="create", settings=settings):
        i = interface.Instance.model_validate({"name": "foo"})
    assert str(i.version) == pg_version
    i = interface.Instance.model_validate({"name": "foo", "version": "15"})
    assert str(i.version) == "15"


def test_instance_auth_options(
    settings: Settings, instance_manifest: interface.Instance
) -> None:
    assert instance_manifest.auth_options(settings.postgresql.auth) == interface.Auth(
        local="peer", host="password", hostssl="trust"
    )


def test_instance_pg_hba(
    settings: Settings,
    instance_manifest: interface.Instance,
    datadir: Path,
    write_changes: bool,
) -> None:
    actual = instance_manifest.pg_hba(settings)
    fpath = datadir / "pg_hba.conf"
    if write_changes:
        fpath.write_text(actual)
    expected = fpath.read_text()
    assert actual == expected


def test_instance_pg_ident(
    settings: Settings,
    instance_manifest: interface.Instance,
    datadir: Path,
    write_changes: bool,
) -> None:
    actual = instance_manifest.pg_ident(settings)
    fpath = datadir / "pg_ident.conf"
    if write_changes:
        fpath.write_text(actual)
    expected = fpath.read_text()
    assert actual == expected


def test_instance_initdb_options(
    settings: Settings, instance_manifest: interface.Instance
) -> None:
    initdb_settings = settings.postgresql.initdb
    assert instance_manifest.initdb_options(initdb_settings) == initdb_settings
    assert instance_manifest.model_copy(
        update={"locale": "X", "data_checksums": True}
    ).initdb_options(initdb_settings) == initdb_settings.model_copy(
        update={"locale": "X", "data_checksums": True}
    )
    assert instance_manifest.model_copy(update={"data_checksums": None}).initdb_options(
        initdb_settings.model_copy(update={"data_checksums": True})
    ) == initdb_settings.model_copy(update={"data_checksums": True})


def test_instance_composite_service(settings: Settings, pg_version: str) -> None:
    pm = plugin_manager(settings)
    Instance = interface.Instance.composite(pm)
    with pytest.raises(
        ValueError,
        match=r"validation error for Instance\nprometheus\n  Input should be a valid dictionary",
    ):
        m = Instance.model_validate(
            {
                "name": "test",
                "version": pg_version,
                "prometheus": None,
                "pgbackrest": {"stanza": "mystanza"},
            }
        )

    m = Instance.model_validate(
        {
            "name": "test",
            "version": pg_version,
            "prometheus": {"port": 123},
            "pgbackrest": {"stanza": "mystanza"},
        }
    )
    s = m.service(prometheus_models.Service)
    assert s.port == 123

    class MyService(types.Service, service_name="notfound"):
        pass

    with pytest.raises(ValueError, match="notfound"):
        m.service(MyService)


def test_role_state() -> None:
    assert interface.Role(name="exist").state == "present"
    assert interface.Role(name="notexist", state="absent").state == "absent"
    assert interface.RoleDropped(name="dropped").state == "absent"
    with pytest.raises(pydantic.ValidationError, match=r"Input should be 'absent'"):
        interface.RoleDropped(name="p", state="present")


def test_database_clone() -> None:
    with pytest.raises(pydantic.ValidationError, match="Input should be a valid URL"):
        interface.Database.model_validate(
            {"name": "cloned_db", "clone": {"dsn": "blob"}}
        )

    expected = {
        "dbname": "base",
        "host": "server",
        "password": "pwd",
        "user": "dba",
    }
    db = interface.Database.model_validate(
        {"name": "cloned_db", "clone": {"dsn": "postgres://dba:pwd@server/base"}}
    )
    assert db.clone is not None
    assert psycopg.conninfo.conninfo_to_dict(str(db.clone.dsn)) == expected


def test_database_schemas_owner() -> None:
    db = interface.Database.model_validate(
        {
            "name": "db",
            "owner": "dba",
            "schemas": ["foo", {"name": "bar", "owner": "postgres"}],
        }
    )
    assert db.model_dump()["schemas"] == [
        {"name": "foo", "owner": "dba"},
        {"name": "bar", "owner": "postgres"},
    ]


def test_connectionstring() -> None:
    assert (
        interface.ConnectionString(conninfo="host=x dbname=y").conninfo
        == "dbname=y host=x"
    )

    with pytest.raises(pydantic.ValidationError, match="contain a password"):
        interface.ConnectionString(conninfo="host=x password=s")


@pytest.mark.parametrize(
    "value, expected",
    [
        ("host=x password=y", {"conninfo": "host=x", "password": "y"}),
        ("host=y", {"conninfo": "host=y", "password": None}),
    ],
)
def test_connectionstring_parse(value: str, expected: dict[str, Any]) -> None:
    parsed = interface.ConnectionString.parse(value)
    assert {
        "conninfo": parsed.conninfo,
        "password": parsed.password.get_secret_value() if parsed.password else None,
    } == expected


@pytest.mark.parametrize(
    "conninfo, password, full_conninfo",
    [
        ("host=x", "secret", "host=x password=secret"),
        ("host=y", None, "host=y"),
    ],
)
def test_connectionstring_full_conninfo(
    conninfo: str, password: str | None, full_conninfo: str
) -> None:
    assert (
        interface.ConnectionString(conninfo=conninfo, password=password).full_conninfo
        == full_conninfo
    )


def test_privileges_sorted() -> None:
    p = interface.Privilege(  # type: ignore[call-arg]
        database="postgres",
        schema="main",
        object_type="table",
        object_name="foo",
        role="postgres",
        privileges=["select", "delete", "update"],
        column_privileges={"postgres": ["update", "delete", "reference"]},
    )
    assert p.model_dump(by_alias=True) == {
        "column_privileges": {"postgres": ["delete", "reference", "update"]},
        "database": "postgres",
        "object_name": "foo",
        "object_type": "table",
        "privileges": ["delete", "select", "update"],
        "role": "postgres",
        "schema": "main",
    }
