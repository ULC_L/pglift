# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import socket
import typing
from dataclasses import dataclass
from unittest.mock import create_autospec

import port_for
import pydantic
import pytest

from pglift import types
from pglift.types import Address, StrEnum, check_port_available, field_annotation


@dataclass(frozen=True)
class MyAnnotation:
    x: str


class M(pydantic.BaseModel):
    x: int
    y: typing.Annotated[str, MyAnnotation("a"), ("a", "b")]


def test_field_annotation() -> None:
    assert field_annotation(M.model_fields["x"], MyAnnotation) is None
    assert field_annotation(M.model_fields["y"], dict) is None
    assert field_annotation(M.model_fields["y"], MyAnnotation) == MyAnnotation("a")
    assert field_annotation(M.model_fields["y"], tuple) == ("a", "b")


def test_check_port_available() -> None:
    p = port_for.select_random()
    info = create_autospec(pydantic.ValidationInfo)
    info.context = {"operation": "create"}
    assert check_port_available(p, info) == p
    with socket.socket() as s:
        s.bind(("", p))
        s.listen()
        with pytest.raises(ValueError, match=f"port {p} already in use"):
            check_port_available(p, info)
        info.context = {"operation": "update"}
        assert check_port_available(p, info) == p


def test_strenum() -> None:
    class Pets(StrEnum):
        cat = "cat"

    assert str(Pets.cat) == "cat"


def test_address() -> None:
    class Cfg(pydantic.BaseModel):
        addr: Address

    cfg = Cfg(addr="server:123")
    assert cfg.addr == "server:123"
    assert types.address_host(cfg.addr) == "server"
    assert types.address_port(cfg.addr) == 123

    a = Address("server:123")
    assert types.address_host(a) == "server"
    assert types.address_port(a) == 123

    # no validation
    assert str(Address("server")) == "server"

    with pytest.raises(pydantic.ValidationError, match="String should match pattern"):
        Cfg(addr="server")
    with pytest.raises(pydantic.ValidationError, match="String should match pattern"):
        Cfg(addr="server:ab")
