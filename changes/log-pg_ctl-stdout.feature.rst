Forward stdout messages from ``pg_ctl`` to our logger when failing to start
the instance. This helps understanding issues when starting PostgreSQL when
log files are not preserved (e.g. at instance creation).
